package ru.geekbrains.fragmentweather;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ru.geekbrains.fragmentweather.publish.Observer;

public class WeatherFragment extends Fragment implements Observer {
    private final String TAG = "myLogs";
    private TextView cityName;
    private ImageView cityImage;

    public WeatherFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        cityName = view.findViewById(R.id.city_name);
        return view;
    }

    @Override
    public void updateText(String text) {
        if (cityName != null) cityName.setText(text);
        Log.i(TAG, "updateText: " + text);
    }
}
