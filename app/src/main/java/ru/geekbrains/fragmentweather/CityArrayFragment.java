package ru.geekbrains.fragmentweather;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Objects;

import ru.geekbrains.fragmentweather.publish.Observer;
import ru.geekbrains.fragmentweather.publish.PublishGetter;
import ru.geekbrains.fragmentweather.publish.Publisher;

public class CityArrayFragment extends ListFragment implements Observer {
    private final String TAG = "myLogs";
    String cities[] = new String[]{"Москва",
            "Санкт-петербург",
            "Новосибирск",
            "Челябинск",
            "Казань",
            "Нижний Новгород",
            "Омск",
            "Самара",
            "Екатеринбург"};
    private Publisher publisher;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                R.layout.support_simple_spinner_dropdown_item, cities);
        setListAdapter(adapter);

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        publisher.notify(cities[position]);
        Log.w(TAG, "onListItemClick: pos = " + cities[position]);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        publisher = ((PublishGetter) context).getPublisher();
    }

    @Override
    public void updateText(String text) {

    }
}
