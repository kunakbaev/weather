package ru.geekbrains.fragmentweather;

public interface Navigator {
    void startSecondFragment(String shape);
}
