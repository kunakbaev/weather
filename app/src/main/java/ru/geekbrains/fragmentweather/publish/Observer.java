package ru.geekbrains.fragmentweather.publish;

public interface Observer {
    void updateText(String text);
}
