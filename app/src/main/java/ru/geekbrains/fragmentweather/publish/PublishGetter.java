package ru.geekbrains.fragmentweather.publish;

public interface PublishGetter {
    Publisher getPublisher();
}
