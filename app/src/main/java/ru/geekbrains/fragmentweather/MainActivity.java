package ru.geekbrains.fragmentweather;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import ru.geekbrains.fragmentweather.publish.PublishGetter;
import ru.geekbrains.fragmentweather.publish.Publisher;

public class MainActivity extends AppCompatActivity implements Navigator, PublishGetter {
    private final String TAG = "myLogs";
    FragmentTransaction fragmentTransaction;
    Publisher publisher = new Publisher();
    private CityArrayFragment cityArrayFragment;
    private WeatherFragment weatherFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e(TAG, "onCreate: ");

        cityArrayFragment = (CityArrayFragment) getSupportFragmentManager().findFragmentById(R.id.poz_array_city);
        weatherFragment = (WeatherFragment) getSupportFragmentManager().findFragmentById(R.id.poz_weather_image);
        publisher.subscribe(weatherFragment);
    }

    @Override
    public void startSecondFragment(String shape) {
//        fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.poz_array_city, weatherFragment);
//        fragmentTransaction.commit();
    }

    @Override
    public Publisher getPublisher() {
        return publisher;
    }
}
